//
//  TabBarViewController.swift
//  TabbarProgramatically
//
//  Created by prudhvi on 12/09/19.
//  Copyright © 2019 prudhvi. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let firstViewController = ViewControllerOne()
        firstViewController.view.backgroundColor = #colorLiteral(red: 0.8549019694, green: 0.250980407, blue: 0.4784313738, alpha: 1)
        firstViewController.tabBarItem = UITabBarItem(tabBarSystemItem: .search, tag: 0)
        //firstViewController.oneLbl.text = "Screen one"
        let secondViewController = ViewControllerTwo()
        secondViewController.tabBarItem = UITabBarItem(tabBarSystemItem: .more, tag: 1)
        secondViewController.view.backgroundColor = #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1)
        let thirdViewController = ViewControllerThree()
        thirdViewController.tabBarItem = UITabBarItem(tabBarSystemItem: .favorites, tag: 2)
        thirdViewController.view.backgroundColor = #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1)
        let tabBarList = [firstViewController, secondViewController, thirdViewController]
        viewControllers = tabBarList
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
